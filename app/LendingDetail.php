<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LendingDetail extends Model
{
    protected $fillable = [
        'lending_id','good_id', 'quantity'
    ];

    public function lending()
    {
        return $this->belongsTo('App\Lending');
    }

    public function good()
    {
        return $this->belongsTo('App\Good');
    }
}
