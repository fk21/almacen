<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Good extends Model
{
    protected $fillable = [
        'office_id','name', 'description', 'code','is_sibsep_code',
        'value','registry_date','active','user_id'
    ];

    public function office()
    {
        return $this->belongsTo('App\Office');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
