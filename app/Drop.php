<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Drop extends Model
{
    protected $fillable = [
        'good_id','office_id', 'chief_id', 'reason','drop_date','user_id'
    ];

    public function good()
    {
        return $this->belongsTo('App\Good');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function office()
    {
        return $this->belongsTo('App\Office');
    }

    public function chief()
    {
        return $this->belongsTo('App\Chief');
    }
}
