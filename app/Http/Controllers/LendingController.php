<?php

namespace App\Http\Controllers;

use Auth;
use Carbon\Carbon;
use App\Lending;
use Illuminate\Http\Request;
use App\LendingDetail;

class LendingController extends Controller
{
    function __construct() {
        $this->middleware('ajax')->only(['index','edit']);
        $this->middleware('auth');
    }

    public function return_lend($id)
    {
        $lend = Lending::where('id',$id)->first();
        return (int) $lend->update([
            'returned_date'=>Carbon::now()
        ]);
    }

    public function index_filter()
    {
        $p = $this->getPermission('lending.create');
        $data = $p->r ? Lending::whereNull('returned_date')->with('user')->get() : [];
        foreach($data as $d)
        $d['p'] = array('a'=>false,'e'=>$p->u,'d'=>$p->d);
        return datatables()->of($data)->toJson();
    }
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $p = $this->getPermission('lending.create');
        $data = $p->r ? Lending::whereNotNull('returned_date')->with('user')->get() : [];
        foreach($data as $d)
        $d['p'] = array('a'=>false,'e'=>$p->u,'d'=>$p->d);
        return datatables()->of($data)->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('lending.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['user_id'] = Auth::user()->id;
        return Lending::create($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Lending  $lending
     * @return \Illuminate\Http\Response
     */
    public function show(Lending $lending)
    {
        $detail = LendingDetail::where('lending_id',$lending->id)->with('good')->get();
        $view = view('lending_detail.show',compact(['lending','detail']))->render();
        return compact('view');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Lending  $lending
     * @return \Illuminate\Http\Response
     */
    public function edit(Lending $lending)
    {
        $view =  view('lending.edit',compact('lending'))->render();
        return compact('view');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Lending  $lending
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Lending $lending)
    {
        return (int)$lending->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Lending  $lending
     * @return \Illuminate\Http\Response
     */
    public function destroy(Lending $lending)
    {
        return (int) $lending->delete();
    }
}
