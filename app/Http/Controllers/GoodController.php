<?php

namespace App\Http\Controllers;

use App\Good;
use App\Office;
use Auth;
use Illuminate\Http\Request;

class GoodController extends Controller
{
    function __construct() {
        $this->middleware('ajax')->only(['index','edit']);
        $this->middleware('auth');
    }

    public function index_filter()
    {
        $p = $this->getPermission('good.create');
        $data = $p->r ? Good::with(['user','office'])->where('active',true)->get() : [];
        foreach($data as $d)
        $d['p'] = array('a'=>false,'e'=>$p->u,'d'=>$p->d);
        return datatables()->of($data)->toJson();
    }
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $p = $this->getPermission('good.create');
        $data = $p->r ? Good::with(['user','office'])->get() : [];
        foreach($data as $d)
        $d['p'] = array('a'=>false,'e'=>$p->u,'d'=>$p->d);
        return datatables()->of($data)->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('good.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['active'] = true;
        $data['user_id'] = Auth::user()->id;
        return Good::create($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Good  $good
     * @return \Illuminate\Http\Response
     */
    public function show(Good $good)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Good  $good
     * @return \Illuminate\Http\Response
     */
    public function edit(Good $good)
    {
        $offices = Office::all();
        $view =  view('good.edit',compact(['good','offices']))->render();
        return compact('view');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Good  $good
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Good $good)
    {
        return (int) $good->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Good  $good
     * @return \Illuminate\Http\Response
     */
    public function destroy(Good $good)
    {
        return (int)$good->delete();
    }
}
