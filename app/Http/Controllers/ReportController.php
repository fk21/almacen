<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PhpOffice\PhpWord\TemplateProcessor;
use App\Drop;
use Carbon\Carbon;
use App\Exchange;
use App\Office;

class ReportController extends Controller
{
    public function trimestral($id)
    {
        $inputs = [];
        switch($id){
            case 1 :
            $inputs = [
                'fecha_inicial'=>'1 de Enero de 2019',
                'fecha_final'=>'31 de Marzo de 2019',
                'b_act'=>'2012',
                'v_act'=>'$387,346.00',
                'b_ant'=>'1723',
                'v_ant'=>'$1,270,676.02',
                'b_nu'=>'100',
                'v_nu'=>'$122,121.00'
            ];
            break;

            case 2:
            $inputs = [
                'fecha_inicial'=>'1 de Abril de 2019',
                'fecha_final'=>'30 de Junio de 2019',
                'b_act'=>'2154',
                'v_act'=>'$456,123.54',
                'b_ant'=>'2012',
                'v_ant'=>'$387,346.00',
                'b_nu'=>'34',
                'v_nu'=>'$12,121.00'
            ];
            break;

            case 3:
            $inputs = [
                'fecha_inicial'=>'1 de Julio de 2019',
                'fecha_final'=>'30 de Septiembre de 2019',
                'b_act'=>'',
                'v_act'=>'',
                'b_ant'=>'',
                'v_ant'=>'',
                'b_nu'=>'',
                'v_nu'=>''
            ];
            break;

            case 4:
            $inputs = [
                'fecha_inicial'=>'1 de Octubre de 2019',
                'fecha_final'=>'31 de Diciembre de 2019'
            ];
            break;
        }
        $url = public_path().'/docs/reporte_trimestral.docx';
        $template = new TemplateProcessor($url);
        foreach ($inputs as $key => $value) {
            $template->setValue($key, $value);
        }
        $template->saveAs('Trimestral.docx');
        return response()->download('Trimestral.docx')->deleteFileAfterSend(true);
    }

    public function baja($id)
    {
        $drop = Drop::where('id',$id)->with(['good','office','chief'])->first();
        $inputs = [
            'fecha'=>Carbon::now()->format('d/m/Y'),
            'dpto'=> $drop->office->department,
            'chief'=>$drop->chief->name,
            'material'=>$drop->good->name
        ];

        $url = public_path().'/docs/baja.docx';
        $template = new TemplateProcessor($url);
        foreach ($inputs as $key => $value) {
            $template->setValue($key, $value);
        }
        $template->saveAs('Baja.docx');
        return response()->download('Baja.docx')->deleteFileAfterSend(true);
    }

    public function transferencia($id)
    {
        $exchange = Exchange::where('id',$id)->with(['good','source','destination'])->first();
        $source = Office::where('id',$exchange->source->id)->with('chief')->first();
        $destination = Office::where('id',$exchange->destination->id)->with('chief')->first();
        $inputs = [
            'fecha'=>Carbon::now()->format('d/m/Y'),
            'source'=>$source->department,
            'destination'=>$destination->department,
            's_chief'=>$source->chief->name,
            'd_chief'=>$destination->chief->name,
            'material'=>$exchange->good->name
        ];

        $url = public_path().'/docs/transferencia.docx';
        $template = new TemplateProcessor($url);
        foreach ($inputs as $key => $value) {
            $template->setValue($key, $value);
        }
        $template->saveAs('Transferencia.docx');
        return response()->download('Transferencia.docx')->deleteFileAfterSend(true);
    }
}
