<?php

namespace App\Http\Controllers;

use App\LendingDetail;
use Illuminate\Http\Request;

class LendingDetailController extends Controller
{
    function __construct() {
       $this->middleware('ajax')->only(['index','edit']);
        $this->middleware('auth');
    }

    public function index_filter($id)
    {
        $p = $this->getPermission('lending_detail.create');
        $data = $p->r ? LendingDetail::where('lending_id',$id)->with('good')->get() : [];
        foreach($data as $d)
        $d['p'] = array('a'=>false,'e'=>false,'d'=>$p->d);
        return datatables()->of($data)->toJson();
    }
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $p = $this->getPermission('lending_detail.create');
        $data = $p->r ? LendingDetail::all() : [];
        foreach($data as $d)
        $d['p'] = array('a'=>false,'e'=>$p->u,'d'=>$p->d);
        return datatables()->of($data)->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('lending_detail.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return LendingDetail::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LendingDetail  $lendingDetail
     * @return \Illuminate\Http\Response
     */
    public function show(LendingDetail $lendingDetail)
    {
        $view = view('lending_detail.show',compact('lendingDetail'))->render();
        return compact('view');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LendingDetail  $lendingDetail
     * @return \Illuminate\Http\Response
     */
    public function edit(LendingDetail $lendingDetail)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LendingDetail  $lendingDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LendingDetail $lendingDetail)
    {
        return (int) $lendingDetail->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LendingDetail  $lendingDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(LendingDetail $lendingDetail)
    {
        return (int) $lendingDetail->delete();
    }
}
