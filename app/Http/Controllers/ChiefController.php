<?php

namespace App\Http\Controllers;

use App\Chief;
use Illuminate\Http\Request;

class ChiefController extends Controller
{
    function __construct() {
        $this->middleware('ajax')->only(['index','edit']);
        $this->middleware('auth');
    }

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $p = $this->getPermission('chief.create');
        $data = $p->r ? Chief::all() : [];
        foreach($data as $d)
        $d['p'] = array('a'=>false,'e'=>$p->u,'d'=>$p->d);
        return datatables()->of($data)->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('chief.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['active']=true;
        return Chief::create($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Chief  $chief
     * @return \Illuminate\Http\Response
     */
    public function show(Chief $chief)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Chief  $chief
     * @return \Illuminate\Http\Response
     */
    public function edit(Chief $chief)
    {
        $view = view('chief.edit',compact('chief'))->render();
        return compact('view');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Chief  $chief
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Chief $chief)
    {
        return (int)$chief->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Chief  $chief
     * @return \Illuminate\Http\Response
     */
    public function destroy(Chief $chief)
    {
        return (int)$chief->delete();
    }
}
