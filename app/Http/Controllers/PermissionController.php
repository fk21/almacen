<?php

namespace App\Http\Controllers;

use App\Permission;
use Illuminate\Http\Request;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Permission::where('parent', '-1')->orderBy('priority', 'ASC')->with('childs')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('permission.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return Permission::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function show(Permission $permission)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $permission = Permission::findOrFail($request['id']);
        return (int)$permission->update($request->all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Permission $permission)
    {
        return (int)$permission->update($request->all());
    }

    public function updatePriority(Request $request)
    {
        return $this->editPriority($request->menu);
    }

    private function editPriority($values, $parent = -1)
    {
        $r = 1;
        foreach ($values as $key => $value) {
            $permission = Permission::findOrFail($value['id']);
            $r = (int)$permission->update(['priority'=>$key+1, 'parent'=>$parent]);
            if(isset($value['children']))
                $r =  $this->editPriority($value['children'], $value['id']);
        }
        return $r;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function destroy(Permission $permission)
    {
        return (int)$permission->delete();
    }
}
