<?php

namespace App\Http\Controllers;

use Auth;
use App\Good;
use App\Drop;
use App\Office;
use Illuminate\Http\Request;

class DropController extends Controller
{
    function __construct() {
        $this->middleware('ajax')->only(['index','edit']);
        $this->middleware('auth');
    }

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $p = $this->getPermission('drop.create');
        $data = $p->r ? Drop::with(['good','office','user','chief'])->get() : [];
        foreach($data as $d)
        $d['p'] = array('a'=>false,'e'=>$p->u,'d'=>$p->d);
        return datatables()->of($data)->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('drop.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $good = Good::where('id',$data['good_id'])->first();
        $good->update([
            'active'=>false
        ]);
        $office = Office::where('id',$good->office_id)->with('chief')->first();

        $data['office_id'] = $office->id;
        $data['chief_id'] = $office->chief->id;
        $data['user_id'] = Auth::user()->id;
        return Drop::create($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Drop  $drop
     * @return \Illuminate\Http\Response
     */
    public function show(Drop $drop)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Drop  $drop
     * @return \Illuminate\Http\Response
     */
    public function edit(Drop $drop)
    {
        $view =  view('drop.edit',compact('drop'))->render();
        return compact('view');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Drop  $drop
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Drop $drop)
    {
        return (int)$drop->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Drop  $drop
     * @return \Illuminate\Http\Response
     */
    public function destroy(Drop $drop)
    {
        return (int) $drop->delete();
    }
}
