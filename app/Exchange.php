<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exchange extends Model
{
    protected $fillable = [
        'good_id','source_id', 'destination_id', 'switch_date','reason',
        'user_id'
    ];

    public function good()
    {
        return $this->belongsTo('App\Good');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function source()
    {
        return $this->belongsTo('App\Office','source_id');
    }

    public function destination()
    {
        return $this->belongsTo('App\Office','destination_id');
    }
}
