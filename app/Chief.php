<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chief extends Model
{
    protected $fillable = [
        'title','rfc', 'name', 'active'
    ];
}
