<?php

namespace App;

use App\Role;
use App\Notifications\ResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','role_id','active','office_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function office()
    {
        return $this->belongsTo('App\Office');
    }

    public function role(){
        return $this->belongsTo(Role::class);
     }

     public function sendPasswordResetNotification($token)
     {
         $this->notify(new ResetPassword($token));
     }
}
