<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Office extends Model
{
    protected $fillable = [
        'department','name', 'chief_id', 'active'
    ];

    public function chief()
    {
        return $this->belongsTo('App\Chief');
    }
}
