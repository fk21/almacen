//Inicializacion
var mod = 'lending_detail';
$(document).ready(function() {
    //dataTable($('#dt'), '/admin/' + mod, Kcolumns);
    drop1();
    drop2();
    //validations();
    //$('#dt').on('click', '.bActive', function() {});
    //$('#dt').on('click', '.bEdit', edt);
    //$('#dt').on('click', '.bDelete', del);
});

//Definición de Tabla
var Kcolumns1 = [{
    data: 'id',
    title: '#'
}, {
    data: 'person_id',
    title: 'Identificación'
}, {
    data: 'due_date',
    title: 'Fecha límite',
    render: dateFormaterTemplate
}, {
    data: 'due_date',
    title: 'Estátus',
    render: function(e) {
        var due_date = new Date(e);
        var today = new Date();

        if (due_date < today) {
            return '<span class="m-badge m-badge--danger m-badge--wide">Vencido</span>';
        } else {
            return '<span class="m-badge m-badge--success m-badge--wide">Activo</span>';
        }
    }
}, {
    data: 'user.name',
    title: 'Usuario'
}, {
    data: 'id',
    title: 'Acciones',
    render: function(e) {
        return '<button type="button" class="btn btn-info" onclick="return_lend(' + e + ')">Entregar</button>' +
            '<button type="button" class="btn btn-success" onclick="show_lend(' + e + ')">Ver Préstamo</button>';
    }
}];

var Kcolumns2 = [{
    data: 'id',
    title: '#'
}, {
    data: 'person_id',
    title: 'Identificación'
}, {
    data: 'due_date',
    title: 'Fecha límite'
}, {
    data: 'user.name',
    title: 'Usuario'
}, {
    data: 'id',
    title: 'Acciones',
    render: function(e) {
        return '<button type="button" class="btn btn-success" onclick="show_lend(' + e + ')">Ver Préstamo</button>';
    }
}];

//Getters

function drop1() {
    $.ajax({
        url: '/admin/flending',
        dataType: 'json',
        success: function(e) {
            dataTableLocal($('#dt1'), e.data, Kcolumns1);
        },
        error: eHandler
    });
}

function drop2() {
    $.ajax({
        url: '/admin/lending',
        dataType: 'json',
        success: function(e) {
            dataTableLocal($('#dt2'), e.data, Kcolumns2);
        },
        error: eHandler
    });
}

function return_lend(id) {
    $.ajax({
        url: '/admin/return/' + id,
        dataType: 'json',
        success: function(e) {
            ShowNotification('Devolución Aceptada', 'success');
            drop1();
            drop2();
        },
        error: eHandler
    });
}

function show_lend(id) {
    $.ajax({
        url: '/admin/lending/' + id,
        dataType: 'json',
        success: function(e) {
            $("#modalContainer").html(e.view);
            $("#eModal").modal();
        },
        error: eHandler
    });
}

function validations() {
    $('#new').validate({
        errorPlacement: ePlacement,
        errorClass: eClass,
        submitHandler(frm) {
            var form = $('#new');
            form.ajaxSubmit({
                url: '/admin/' + mod,
                method: 'POST',
                success: function(response, status, xhr, $form) {
                    ShowNotification('Registro Exitoso', 'success');
                    dt.ajax.reload();
                    form[0].reset();
                },
                error: eHandler
            });
        }
    });
}

//Edición

var edid;

function edt() {
    edid = $(this).data('id');
    $.ajax({
        url: '/admin/' + mod + '/' + edid + '/edit',
        dataType: 'json',
        success: function(e) {
            $("#modalContainer").html(e.view);
            $("#eModal").modal();
            $(".sSpecial").select2({ placeholder: 'Seleccione un Rol de Usuario', width: '100%' });
            update();
        },
        error: eHandler
    });
}

function update() {
    $('#editForm').validate({
        errorPlacement: ePlacement,
        errorClass: eClass,
        submitHandler(frm) {
            var form = $('#editForm');
            form.ajaxSubmit({
                url: '/admin/' + mod + '/' + edid,
                method: 'POST',
                data: { _method: 'PUT' },
                success: function(response, status, xhr, $form) {
                    ShowNotification('Actualización Exitosa', 'success');
                    dt.ajax.reload();
                    $("#eModal").modal('toggle');
                },
                error: eHandler
            });
        }
    });
}

//Eliminación

var delid;

function del() {
    delid = $(this).data('id');
    swal({
        title: "¿Seguro que deseas eliminar este registro?",
        text: "No habrá manera de revertir esta acción",
        type: "warning",
        showCancelButton: !0,
        confirmButtonText: "Si, Elimínalo",
        cancelButtonText: "Cancelar",
        confirmButtonClass: "btn btn-danger m-btn--pill m-btn--air",
        cancelButtonClass: "btn btn-outline-default m-btn--pill m-btn--air"
    }).
    then(delHandler);
}

function delHandler(r) {
    if (r.value) {
        $.ajax({
            url: '/admin/' + mod + '/' + delid,
            method: 'POST',
            data: { _token: $("[name='_token']").val(), _method: 'DELETE' },
            dataType: 'json',
            success: function(e) {
                ShowNotification('Registro Eliminado Satisfactoriamente', 'success');
                dt.ajax.reload();
            },
            error: eHandler
        });
    }
}

function dataTableLocal(tbl, data, columns) {
    dt = tbl.DataTable({
        language: {
            processing: "Procesando...",
            search: "Buscar&nbsp;:",
            lengthMenu: "Mostrar _MENU_ ",
            info: "Mostrando de _START_ al _END_ de un total de _TOTAL_ registros",
            infoEmpty: "No hay datos disponibles",
            infoFiltered: "(Filtrando _MAX_ registros)",
            infoPostFix: "",
            loadingRecords: "Obteniendo Datos...",
            zeroRecords: "No hay datos disponibles para su búsqueda",
            emptyTable: "No hay datos disponibles",
            aria: {
                sortAscending: ": Ordenar Ascendente",
                sortDescending: ": Ordenar Descendente"
            }
        },
        data: data,
        destroy: true,
        responsive: true,
        dom: "<'row'<'col-sm-6 text-left'f><'col-sm-6 text-right'B>>\n\t\t\t<'row'<'col-sm-12'tr>>\n\t\t\t<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",
        buttons: [
            { extend: "print", text: "Imprimir" },
            { extend: "copyHtml5", text: "Copiar" },
            "excelHtml5",
            "pdfHtml5"
        ],
        columns: columns,
        drawCallback: function() {
            $(".ttip").tooltip();
        }
    });

    return dt;
}