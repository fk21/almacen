//Inicializacion
var mod = 'lending';
$(document).ready(function() {
    //dataTable($('#dt'), '/admin/' + mod, Kcolumns);
    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd',
        width: '100%'
    });
    $('#new1').hide();
    $('#student').select2({
        placeholder: '¿Es un alumno? ',

    });
    drop1();
    validations();
    $('#dt').on('click', '.bActive', function() {});
    $('#dt').on('click', '.bEdit', edt);
    $('#dt').on('click', '.bDelete', del);
});

//Definición de Tabla
var Kcolumns = [{
        data: 'id',
        title: '#'
    }, {
        data: 'good.name',
        title: 'Material'
    }, {
        data: 'quantity',
        title: 'Cantidad'
    }, {
        data: 'created_at',
        title: 'Creado',
        render: dateFormaterTemplate
    }, {
        data: 'updated_at',
        title: 'Actualizado',
        render: dateFormaterTemplate
    },
    {
        data: 'p',
        title: 'Acciones',
        render: renderActions
    }
];

//Getters

function drop1() {
    $.ajax({
        url: '/admin/fgood',
        dataType: 'json',
        success: function(e) {
            $('#good').select2({
                placeholder: 'Seleccione un Material',
                data: $.map(e.data, function(e) {
                    e.text = e.name;
                    return e;
                })
            });
        },
        error: eHandler
    });
}

var lend_id;

function validations() {
    $('#new').validate({
        errorPlacement: ePlacement,
        errorClass: eClass,
        submitHandler(frm) {
            var form = $('#new');
            form.ajaxSubmit({
                url: '/admin/' + mod,
                method: 'POST',
                success: function(response, status, xhr, $form) {
                    ShowNotification('Ahora proceda a agregar los artículos', 'success');
                    $('#new').hide();
                    $('#new1').show();
                    lend_id = response.id

                },
                error: eHandler
            });
        }
    });

    $('#new1').validate({
        errorPlacement: ePlacement,
        errorClass: eClass,
        submitHandler(frm) {
            var d = {
                _token: $('meta[name="csrf-token"]').attr('content'),
                good_id: $('#good').val(),
                lending_id: lend_id,
                quantity: $('#quantity').val()
            };
            $.ajax({
                url: '/admin/lending_detail',
                method: 'POST',
                data: d,
                dataType: 'json',
                success: function(e) {
                    ShowNotification('Artículo Agregado', 'success');
                    getData();
                }
            });
        }
    });
}

function getData() {
    $.ajax({
        url: '/admin/flending_detail/' + lend_id,
        method: 'GET',
        dataType: 'json',
        success: function(e) {
            dataTable(e.data);
        }
    });
}
//Edición

var edid;

function edt() {
    edid = $(this).data('id');
    $.ajax({
        url: '/admin/' + mod + '/' + edid + '/edit',
        dataType: 'json',
        success: function(e) {
            $("#modalContainer").html(e.view);
            $("#eModal").modal();
            $(".sSpecial").select2({ placeholder: 'Seleccione un Rol de Usuario', width: '100%' });
            update();
        },
        error: eHandler
    });
}

function update() {
    $('#editForm').validate({
        errorPlacement: ePlacement,
        errorClass: eClass,
        submitHandler(frm) {
            var form = $('#editForm');
            form.ajaxSubmit({
                url: '/admin/' + mod + '/' + edid,
                method: 'POST',
                data: { _method: 'PUT' },
                success: function(response, status, xhr, $form) {
                    ShowNotification('Actualización Exitosa', 'success');
                    dt.ajax.reload();
                    $("#eModal").modal('toggle');
                },
                error: eHandler
            });
        }
    });
}

//Eliminación

var delid;

function del() {
    delid = $(this).data('id');
    swal({
        title: "¿Seguro que deseas eliminar este registro?",
        text: "No habrá manera de revertir esta acción",
        type: "warning",
        showCancelButton: !0,
        confirmButtonText: "Si, Elimínalo",
        cancelButtonText: "Cancelar",
        confirmButtonClass: "btn btn-danger m-btn--pill m-btn--air",
        cancelButtonClass: "btn btn-outline-default m-btn--pill m-btn--air"
    }).
    then(delHandler);
}

function delHandler(r) {
    if (r.value) {
        $.ajax({
            url: '/admin/lending_detail/' + delid,
            method: 'POST',
            data: { _token: $("[name='_token']").val(), _method: 'DELETE' },
            dataType: 'json',
            success: function(e) {
                ShowNotification('Registro Eliminado Satisfactoriamente', 'success');
                //dt.ajax.reload();
                getData();
            },
            error: eHandler
        });
    }
}


function dataTable(data) {
    dt = $('#dt').DataTable({
        language: {
            processing: "Procesando...",
            search: "Buscar&nbsp;:",
            lengthMenu: "Mostrar _MENU_ ",
            info: "Mostrando de _START_ al _END_ de un total de _TOTAL_ registros",
            infoEmpty: "No hay datos disponibles",
            infoFiltered: "(Filtrando _MAX_ registros)",
            infoPostFix: "",
            loadingRecords: "Obteniendo Datos...",
            zeroRecords: "No hay datos disponibles para su búsqueda",
            emptyTable: "No hay datos disponibles",
            aria: {
                sortAscending: ": Ordenar Ascendente",
                sortDescending: ": Ordenar Descendente"
            }
        },
        data: data,
        destroy: true,
        responsive: true,
        dom: "<'row'<'col-sm-6 text-left'f><'col-sm-6 text-right'B>>\n\t\t\t<'row'<'col-sm-12'tr>>\n\t\t\t<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",
        buttons: [
            { extend: "print", text: "Imprimir" },
            { extend: "copyHtml5", text: "Copiar" },
            "excelHtml5",
            "pdfHtml5"
        ],
        columns: Kcolumns,
        drawCallback: function() {
            $(".ttip").tooltip();
        }
    });

    return dt;
}