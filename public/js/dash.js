$(document).ready(function() {
    $('#report').select2({
        placeholder: 'Seleccione el tipo de reporte'
    });
    getGraph();
    var data = [
        [1, 'Ene-Mar', 1],
        [2, 'Abr-Jun', 2],
        [3, 'Jul-Sep', 3],
        [4, 'Oct-Dic', 4]
    ];
    dataTableLocal($('#dt'), data, Kcolumns);
});

var Kcolumns = [{
    title: 'Trimestre'
}, {
    title: 'Fechas'
}, {
    title: 'Reporte',
    render: function(e) {
        return '<button type="button" class="btn btn-info" onclick="report(' + e + ')">Descargar</button>';
    }
}];


function getGraph(div, options) {

    var myChart = echarts.init(document.getElementById('graph'));

    var labelOption = {
        normal: {
            show: true,
            position: 'insideBottom',
            distance: 5,
            align: 'left',
            verticalAlign: 'middle',
            rotate: 90,
            formatter: '{c}  {name|{a}}',
            fontSize: 12,
            rich: {
                name: {
                    textBorderColor: '#FFF'
                }
            }
        }
    };

    option = {
        //color: ['#003366', '#006699', '#4cabce'],
        tooltip: {
            trigger: 'axis',
            axisPointer: {
                type: 'shadow'
            }
        },
        legend: {
            data: ['Entradas', 'Bajas', 'Transferencias']
        },
        toolbox: {
            show: true,
            orient: 'vertical',
            left: 'right',
            top: 'center',
            feature: {
                mark: { show: true },
                magicType: {
                    show: true,
                    type: ['line', 'bar', 'stack', 'tiled'],
                    'title': { 'line': 'Líneas', 'bar': 'Barras', 'stack': 'Apilado', 'tiled': 'Barras' }
                },
                saveAsImage: { show: true, title: 'Guardar' }
            }
        },
        calculable: true,
        xAxis: [{
            type: 'category',
            axisTick: { show: false },
            data: ['Ene-Mar', 'Abr-Jun', 'Jul-Sep', 'Oct-Dic']
        }],
        yAxis: [{
            type: 'value'
        }],
        series: [{
                name: 'Entradas',
                type: 'bar',
                barGap: 0,
                label: labelOption,
                data: [123, 62, 0, 0]
            },
            {
                name: 'Bajas',
                type: 'bar',
                label: labelOption,
                data: [0, 181, 0, 0]
            },
            {
                name: 'Transferencias',
                type: 'bar',
                label: labelOption,
                data: [30, 12, 0, 0]
            }
        ]
    };
    myChart.setOption(option);
}

function report(id) {
    window.open("/admin/report/trimestral/" + id, "_blank");
}

function dataTableLocal(tbl, data, columns) {
    dt = tbl.DataTable({
        language: {
            processing: "Procesando...",
            search: "Buscar&nbsp;:",
            lengthMenu: "Mostrar _MENU_ ",
            info: "Mostrando de _START_ al _END_ de un total de _TOTAL_ registros",
            infoEmpty: "No hay datos disponibles",
            infoFiltered: "(Filtrando _MAX_ registros)",
            infoPostFix: "",
            loadingRecords: "Obteniendo Datos...",
            zeroRecords: "No hay datos disponibles para su búsqueda",
            emptyTable: "No hay datos disponibles",
            aria: {
                sortAscending: ": Ordenar Ascendente",
                sortDescending: ": Ordenar Descendente"
            }
        },
        data: data,
        destroy: true,
        responsive: true,
        dom: "<'row'<'col-sm-6 text-left'f><'col-sm-6 text-right'B>>\n\t\t\t<'row'<'col-sm-12'tr>>\n\t\t\t<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",
        buttons: [
            { extend: "print", text: "Imprimir" },
            { extend: "copyHtml5", text: "Copiar" },
            "excelHtml5",
            "pdfHtml5"
        ],
        columns: columns,
        drawCallback: function() {
            $(".ttip").tooltip();
        }
    });

    return dt;
}