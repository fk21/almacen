//Inicializacion
var mod = 'good';
$(document).ready(function() {
    dataTable($('#dt'), '/admin/' + mod, Kcolumns);
    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd'
    });
    $('#sibsep').select2({
        placeholder: "¿Es un código SIBSEP?"
    });
    drop1();
    validations();
    $('#dt').on('click', '.bActive', function() {});
    $('#dt').on('click', '.bEdit', edt);
    $('#dt').on('click', '.bDelete', del);
});

//Definición de Tabla
var Kcolumns = [{
        data: 'id',
        title: '#'
    }, {
        data: 'office.name',
        title: 'Oficina'
    }, {
        data: 'name',
        title: 'Material'
    }, {
        data: 'description',
        title: 'Descripcion'
    }, {
        data: 'code',
        title: 'Código'
    }, {
        data: 'is_sibsep_code',
        title: 'SIBSEP',
        render: function(e) {
            return e == 0 ? 'No' : 'Si';
        }
    }, {
        data: 'value',
        title: 'Valor Monetario'
    }, {
        data: 'registry_date',
        title: 'Fecha de Registro'
    }, {
        data: 'user.name',
        title: 'Registró'
    }, {
        data: 'active',
        title: 'Activo',
        render: function(e) {
            var r;
            if (e == 1) {
                r = '<span class="m-badge m-badge--success m-badge--wide">Activo</span>';
            } else {
                r = '<span class="m-badge m-badge--danger m-badge--wide">Inactivo</span>';
            }
            return r;
        }
    }, {
        data: 'created_at',
        title: 'Creado',
        render: dateFormaterTemplate
    }, {
        data: 'updated_at',
        title: 'Actualizado',
        render: dateFormaterTemplate
    },
    {
        data: 'p',
        title: 'Acciones',
        render: renderActions
    }
];

//Getters

function drop1() {
    $.ajax({
        url: '/admin/office',
        dataType: 'json',
        success: function(e) {
            $('#office').select2({
                placeholder: 'Seleccione una Oficina',
                data: $.map(e.data, function(e) {
                    e.text = e.name;
                    return e;
                })
            });
        },
        error: eHandler
    });
}

function validations() {
    $('#new').validate({
        errorPlacement: ePlacement,
        errorClass: eClass,
        submitHandler(frm) {
            var form = $('#new');
            form.ajaxSubmit({
                url: '/admin/' + mod,
                method: 'POST',
                success: function(response, status, xhr, $form) {
                    ShowNotification('Registro Exitoso', 'success');
                    dt.ajax.reload();
                    form[0].reset();
                },
                error: eHandler
            });
        }
    });
}

//Edición

var edid;

function edt() {
    edid = $(this).data('id');
    $.ajax({
        url: '/admin/' + mod + '/' + edid + '/edit',
        dataType: 'json',
        success: function(e) {
            $("#modalContainer").html(e.view);
            $("#eModal").modal();
            $('.datepicker').datepicker({
                format: 'yyyy-mm-dd'
            });
            $(".sSpecial").select2({ placeholder: 'Seleccione un Rol de Usuario', width: '100%' });
            update();
        },
        error: eHandler
    });
}

function update() {
    $('#editForm').validate({
        errorPlacement: ePlacement,
        errorClass: eClass,
        submitHandler(frm) {
            var form = $('#editForm');
            form.ajaxSubmit({
                url: '/admin/' + mod + '/' + edid,
                method: 'POST',
                data: { _method: 'PUT' },
                success: function(response, status, xhr, $form) {
                    ShowNotification('Actualización Exitosa', 'success');
                    dt.ajax.reload();
                    $("#eModal").modal('toggle');
                },
                error: eHandler
            });
        }
    });
}

//Eliminación

var delid;

function del() {
    delid = $(this).data('id');
    swal({
        title: "¿Seguro que deseas eliminar este registro?",
        text: "No habrá manera de revertir esta acción",
        type: "warning",
        showCancelButton: !0,
        confirmButtonText: "Si, Elimínalo",
        cancelButtonText: "Cancelar",
        confirmButtonClass: "btn btn-danger m-btn--pill m-btn--air",
        cancelButtonClass: "btn btn-outline-default m-btn--pill m-btn--air"
    }).
    then(delHandler);
}

function delHandler(r) {
    if (r.value) {
        $.ajax({
            url: '/admin/' + mod + '/' + delid,
            method: 'POST',
            data: { _token: $("[name='_token']").val(), _method: 'DELETE' },
            dataType: 'json',
            success: function(e) {
                ShowNotification('Registro Eliminado Satisfactoriamente', 'success');
                dt.ajax.reload();
            },
            error: eHandler
        });
    }
}