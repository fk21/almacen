"use strict;"

var Permission = function() {
    var ol;
    var mod = 'permission';

    function editar(form) {
        $(form).ajaxSubmit({
            url: '/admin/' + mod + '/' + $(form).find('#id').val(),
            method: 'POST',
            data: { _method: 'PUT', _token: $("[name='_token']").val(), _method: 'PUT' },
            success: function(response, status, xhr, $form) {
                ShowNotification('Actualización Exitosa', 'success');
                prepareMenuBuilder();
            },
            error: eHandler
        });
    }
    var delid;

    function eliminar() {
        var that = $(this);
        delid = $(this).data('id');
        swal({
            title: "¿Seguro que deseas eliminar este registro?",
            text: "No habrá manera de revertir esta acción",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Si, Elimínalo",
            cancelButtonText: "Cancelar",
            confirmButtonClass: "btn btn-danger m-btn--pill m-btn--air",
            cancelButtonClass: "btn btn-outline-default m-btn--pill m-btn--air"
        }).
        then(deleteMenu);
    }

    function deleteMenu() {
        $.ajax({
            url: '/admin/' + mod + '/' + delid,
            method: 'POST',
            data: { _token: $("[name='_token']").val(), _method: 'DELETE' },
            dataType: 'json',
            success: function(e) {
                ShowNotification('Registro Eliminado Satisfactoriamente', 'success');
                prepareMenuBuilder();
            },
            error: eHandler
        });
    }

    function editarFn(e) {
        var target = $(e.delegateTarget).parent().siblings('form');
        $("[name*='editForm_']").not("[name*='" + target.attr('name') + "']").slideUp(300);
        $("[name*='editForm_']").not("[name*='" + target.attr('name') + "']").data('validator', null);
        $("[name*='editForm_']").not("[name*='" + target.attr('name') + "']").unbind('validate');
        target.slideToggle(300);
        $("[name*='editForm_'] [name*='icon_']").each(function() {
            if ($(this).hasClass("select2-hidden-accessible"))
                $(this).select2('destroy');
        });

        setupEvents($(target), editar);
    }

    function menuChange() {
        vid = $(this).data('id');
        $.ajax({
            url: '/admin/permission/priority',
            dataType: 'json',
            method: 'POST',
            data: { _method: 'PUT', _token: $("[name='_token']").val(), _method: 'PUT', menu: ol.nestedSortable('toHierarchy') },
            success: function(e) {
                prepareMenuBuilder();
            },
            error: eHandler
        });
    }

    function prepareMenuBuilder() {
        var d = new Date();
        d = '?v=' + d.getTime();

        $.ajax({
            url: '/admin/permission',
            dataType: 'json',
            error: eHandler
        }).then(function(e) {
            var t = $.Deferred();
            var p = $.Deferred();
            $.get('/Mviews/menu_builder.html' + d, function(template) {
                t.resolve(template);
            });
            $.get('/Mviews/menu_builder_partial.html' + d, function(partial) {
                p.resolve(partial);
            });
            $.when(t, p).then(function(template, partial) {
                var rendered = Mustache.render(template, {
                    childs: e,
                    hasChilds: function() {
                        return this.childs && this.childs.length > 0;
                    },
                    isRoot: function() {
                        return this.parent == -1;
                    },
                    isSection: function() {
                        return this.menu == 0;
                    },
                    iconName: function() {
                        var iconArr = this.icon.split(' ');
                        return iconArr.length > 1 ? iconArr[iconArr.length - 1] : this.icon;
                    }
                }, {
                    child: partial
                });
                $('#menu_builder').html(rendered);
            }).then(initMenuBuilder);
        });
    }

    function initMenuBuilder() {
        ol = $('ol.sortable').nestedSortable({
            forcePlaceholderSize: true,
            handle: 'div.handle',
            helper: 'clone',
            items: 'li.item',
            opacity: .6,
            isTree: true,
            errorClass: 'placeholder-error',
            placeholder: 'placeholder',
            tabSize: 15,
            tolerance: 'pointer',
            toleranceElement: '> div',
            maxLevels: 3,
            excludeRoot: true,
            expandOnHover: 700,
            startCollapsed: false,
            isAllowed: function(item, parent, sorted) {
                if (sorted.attr("data-type") != 0) {
                    if (parent != undefined && parent.attr("data-type") == 0)
                        return false;
                    return true;
                } else if (sorted.attr("data-type") == 0 && parent == undefined)
                    return true;
                return false;
            },
            relocate: menuChange
        });
        $('.m-portlet__head-tools').on('click', '.disclose', function() {
            $(this).parent().parent().parent().closest('li').toggleClass('mjs-nestedSortable-collapsed').toggleClass('mjs-nestedSortable-expanded');
            $(this).children('i').toggleClass('la-angle-down').toggleClass('la-angle-up');
        });
        $('.m-portlet__head-tools').on('click', '.delete', eliminar);
        $('.m-portlet__head-tools').on('click', '.edit', editarFn);
    }

    function setupEvents(form, handler) {
        form.find("#icon").select2({
            minimumInputLength: 2,
            language: "es",
            ajax: {
                url: "/assets/app/json/icons.php",
                dataType: "json",
                delay: 250,
                type: "GET",
                data: function(params) {
                    var queryParameters = {
                        query: params.term
                    }
                    return queryParameters;
                },
                processResults: function(data) {
                    return {
                        results: data
                    };
                }
            },
            templateResult: format,
            templateSelection: format
        });

        var rules = {
            menu: {
                required: true,
                range: [0, 1]
            },
            title: {
                required: true
            },
            route: {
                required: '#type_menu:checked'
            },
            icon: {
                required: '#type_menu:checked'
            }
        };
        form.validate({
            rules: rules,
            errorPlacement: ePlacement,
            errorClass: eClass,
            submitHandler: handler
        });
    }

    function create(frm) {
        var form = $('#new');
        form.ajaxSubmit({
            url: '/admin/' + mod,
            method: 'POST',
            success: function(response, status, xhr, $form) {
                prepareMenuBuilder();
                ShowNotification('Registro Exitoso', 'success');
                form[0].reset();
            },
            error: eHandler
        });
    }

    function aliasFn(menu) {
        return menu != undefined ? menu.val().normalize('NFD').toLowerCase().replace(/[\u0300-\u036f]/g, "").replace(/\s+/g, '_') : '';
    }

    function urlFn(router, menu) {
        return menu.val() && router.val() ? '#/' + router.val() + '/' + menu.val() : '';
    }

    function format(item) {
        return $('<div><i style="margin-right: 12px; " class=" ' + item.id + ' "></i>' + item.text + '</div>');
    }

    return {
        init: function() {
            prepareMenuBuilder();
            setupEvents($("#new"), create);
        }
    }
}();

$(document).ready(function() {
    Permission.init();
});