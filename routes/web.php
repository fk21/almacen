<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/test',function () {
    $permissions = App\Permission_Role::where('role_id',1)->with('permission')->get();
    $parentMenus = Illuminate\Support\Collection::make();
    $childMenus = Illuminate\Support\Collection::make();
    foreach($permissions as $permission){
        if($permission['permission']['parent'] == -1)
            $parentMenus->push($permission['permission']);
        else
            $childMenus->push($permission['permission']);
    }
    return compact('parentMenus','childMenus');
});

Auth::routes();
Route::get('/', function () {
    return redirect()->route('login');
});
Route::get('/login',function () {
    return view('auth.login');
})->name('login');

Route::get('/admin/dash', 'DashController@index')->name('dash');
Route::resource('/admin/user','UserController');
Route::resource('/admin/role','RoleController');
Route::put('/admin/permission/priority','PermissionController@updatePriority')->name('permission.priotity');
Route::resource('/admin/permission','PermissionController');
Route::resource('/admin/good','GoodController');
Route::resource('/admin/exchange','ExchangeController');
Route::resource('/admin/chief','ChiefController');
Route::resource('/admin/lending','LendingController');
Route::resource('/admin/lending_detail','LendingDetailController');
Route::resource('/admin/office','OfficeController');
Route::resource('/admin/drop','DropController');
Route::get('admin/department','OfficeController@departments');
Route::get('admin/fgood','GoodController@index_filter');
Route::get('admin/flending_detail/{id}','LendingDetailController@index_filter');
Route::get('admin/flending','LendingController@index_filter');
Route::get('admin/return/{id}','LendingController@return_lend');

Route::get('admin/report/trimestral/{id}','ReportController@trimestral');
Route::get('admin/report/transferencia/{id}','ReportController@transferencia');
Route::get('admin/report/baja/{id}','ReportController@baja');
