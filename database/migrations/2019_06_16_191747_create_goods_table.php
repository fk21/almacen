<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGoodsTable extends Migration
{
    /**
     * [
        'office_id','name', 'description', 'code','is_sibsep_code',
        'value','registry_date','active','user_id'
        ];
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('goods', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('office_id');
            $table->string('name');
            $table->text('description');
            $table->string('code');
            $table->boolean('is_sibsep_code');
            $table->decimal('value',10,2);
            $table->date('registry_date');
            $table->boolean('active');
            $table->integer('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('goods');
    }
}
