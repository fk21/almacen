<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLendingDetailsTable extends Migration
{
    /**
     * [
        'lending_id','good_id', 'quantity'
    ];
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lending_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('lending_id');
            $table->integer('good_id');
            $table->integer('quantity');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lending_details');
    }
}
