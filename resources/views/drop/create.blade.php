@extends('layouts.admin')
@section('scripts')
<script src="/js/drop.js"></script>
@stop
@section('content')
@if($crud->c)
<div class="row">
    <div class="col-md-12">
        <div class="m-portlet">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon">
                            <i class="fa fa-remove"></i>
                        </span>
                        <h3 class="m-portlet__head-text">
                            Baja de Materiales
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <a href="#" class="m-portlet__nav-link m-portlet__nav-link--icon">
                                <i class="la la-angle-down"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <form action="" id="new">
                <div class="m-portlet__body">

                    @csrf
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group m-form__group">
                                <select name="good_id" class="form-control m-select2" required id="good">
                                    <option value=""></option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group m-form__group">
                                <div class="m-input-icon m-input-icon--left">
                                    <input type="text" class="form-control m-input datepicker" required placeholder="Fecha de Baja" name="drop_date">
                                    <span class="m-input-icon__icon m-input-icon__icon--left">
                                        <span>
                                            <i class="fa flaticon-time-2"></i>
                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group m-form__group">
                                <div class="m-input-icon m-input-icon--left">
                                    <input type="text" class="form-control m-input" required placeholder="Motivo" name="reason">
                                    <span class="m-input-icon__icon m-input-icon__icon--left">
                                        <span>
                                            <i class="fa fa-question"></i>
                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <span></span>

                </div>
                <div class="m-portlet__foot">
                    <div class="m-form__actions">
                        <button type="submit" class="btn btn-info m-btn--pill m-btn--air">Registrar</button>
                        <button type="reset" class="btn btn-outline-danger m-btn--pill m-btn--air">Cancelar</button>

                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endif
@if($crud->r)
<div class="row">
    <div class="col-md-12">
        <div class="m-portlet">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon">
                            <i class="fa fa-remove"></i>
                        </span>
                        <h3 class="m-portlet__head-text">
                            Bajas Registrados
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <a href="#" class="m-portlet__nav-link m-portlet__nav-link--icon">
                                <i class="la la-angle-down"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="m-portlet__body">
                    <table id="dt" class="table table-striped- table-bordered table-hover table-checkable">

                    </table>
                <div class="m_datatable">

                </div>
            </div>
        </div>
    </div>
</div>
@endif
<div id="modalContainer">

</div>

@stop
