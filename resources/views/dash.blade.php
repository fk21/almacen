@extends('layouts.admin')
@section('title')
Gráficos y Reportes
@stop
@section('scripts')
<script src="/js/echarts.min.js"></script>
<script src="/js/dash.js"></script>
@stop

@section('content')

<div class="row">
        <div class="col-md-12">
            <div class="m-portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon">
                                <i class="flaticon-user"></i>
                            </span>
                            <h3 class="m-portlet__head-text" id="graph-title">
                                Gráfico
                            </h3>
                        </div>
                    </div>
                    <div class="m-portlet__head-tools">
                        <ul class="m-portlet__nav">
                            <li class="m-portlet__nav-item">
                                <a href="#" class="m-portlet__nav-link m-portlet__nav-link--icon">
                                    <i class="la la-angle-down"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <div id="graph" style="height:500px">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
            <div class="col-md-12">
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon">
                                    <i class="flaticon-users"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Reportes Trimestrales
                                </h3>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                <li class="m-portlet__nav-item">
                                    <a href="#" class="m-portlet__nav-link m-portlet__nav-link--icon">
                                        <i class="la la-angle-down"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                            <table id="dt" class="table table-striped- table-bordered table-hover table-checkable">

                            </table>
                    </div>
                </div>
            </div>
        </div>


<div id="modalContainer">

</div>

@stop
