<!--begin::Modal-->
<div class="modal fade" id="eModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    Editar Registro
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <form id="editForm">
                <div class="modal-body">
                    @csrf
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group m-form__group">
                                <div class="m-input-icon m-input-icon--left">

                                    <input type="text" value="{{ $chief->title }}" class=" form-control form-control-danger m-input" required placeholder="Título" name="title">
                                    <span class="m-input-icon__icon m-input-icon__icon--left">
                                        <span>
                                            <i class="la la-user"></i>
                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group m-form__group">
                                <div class="m-input-icon m-input-icon--left">
                                    <input type="text" value="{{ $chief->name }}" class="form-control m-input" required placeholder="Nombre" name="name">
                                    <span class="m-input-icon__icon m-input-icon__icon--left">
                                        <span>
                                            <i class="la la-send"></i>
                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group m-form__group">
                                <div class="m-input-icon m-input-icon--left">
                                    <input type="text" value="{{ $chief->rfc }}" class="form-control m-input" required placeholder="RFC" name="rfc">
                                    <span class="m-input-icon__icon m-input-icon__icon--left">
                                        <span>
                                            <i class="la la-send"></i>
                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success m-btn--pill m-btn--air">
                        Editar Registro
                    </button>
                    <button type="button" class="btn btn-outline-danger m-btn--pill m-btn--air" data-dismiss="modal">
                            Cancelar
                        </button>
                </div>
            </form>
        </div>
    </div>
</div>
<!--end::Modal-->
