<div class="modal fade" id="eModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    Detalle del Préstamo
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <form id="editForm">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
                                <div class="m-demo__preview">
                                    <div class="m-list-timeline">
                                        <div class="m-list-timeline__items">
                                            @foreach($detail as $d)
                                            <div class="m-list-timeline__item">
                                                <span
                                                    class="m-list-timeline__badge m-list-timeline__badge--success">
                                                </span>
                                                <span class="m-list-timeline__text">
                                                    {{ $d->good->name }}
                                                </span>
                                                <span class="m-list-timeline__time">
                                                    {{ $d->quantity }}
                                                </span>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-info m-btn--pill m-btn--air" data-dismiss="modal">
                        OK
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
<!--end::Modal-->
